﻿using System;
using System.Collections.Generic;
using System.Text;

namespace App1
{
    public class PointOfInterest
    {
        public int? Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Address { get; set; }
        public double? Latitude { get; set; }
        public double? Longitude { get; set; }
    }
}
